"""
このファイルに解答コードを書いてください
"""
#!/usr/bin/python
# coding: UTF-8
f = open('input.txt')
lines = f.read()
lines1 = lines.split('\n')
stop = len(lines1) - 2
m = int(lines1[-2])
count = 0
dict_input = {}
for line in lines1:
    if count == stop:
        break
    i_and_s = line.split(':')
    i = i_and_s[0]
    s = i_and_s[1]
    dict_input['{}'.format(i)] = s
    count += 1
dict2 = sorted(dict_input.items(), key=lambda x:x[1])

final_word = ''
judge = 0
for i_and_s in dict2:
    i, s = i_and_s
    i = int(i)
    s = str(s)
    if m % i == 0:
        final_word = final_word + s
    else:
        if i % 3 == 0 and 1 % 5 == 0:
            final_word = final_word + "FizzBuzz"
            judge += 1
        else:
            if i % 3 == 0:
                final_word = final_word + "Fizz"
                judge += 1
            elif i % 5 == 0:
                final_word = final_word + "Buzz"
                judge += 1

if judge == 0:
    print(m)
else:
    print(final_word)